using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Gioco_open_day_2
{
    /// <summary>
    /// Logica di interazione per MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DispatcherTimer timer = new DispatcherTimer();
        DispatcherTimer disegno = new DispatcherTimer();
        DispatcherTimer Controllo = new DispatcherTimer();
        DispatcherTimer animazioni = new DispatcherTimer();

        TransformGroup ts = new TransformGroup();
        RotateTransform RotateTransform = new RotateTransform();
        TranslateTransform translateTransform = new TranslateTransform();
        bool rotazione = false;
        // scaleTransorm 
        public MainWindow()
        {
            timer.Interval = TimeSpan.FromMilliseconds(1);
            timer.Tick += Timer_Tick;
            disegno.Interval = TimeSpan.FromMilliseconds(1);
            disegno.Tick += Disegno_Tick;
            animazioni.Interval = TimeSpan.FromMilliseconds(900);
            animazioni.Tick += Animazioni_Tick;
            Controllo.Interval = TimeSpan.FromMilliseconds(1);
            Controllo.Tick += Controllo_Tick;
            

            InitializeComponent();
            WindowState = WindowState.Maximized;
            campo.KeyDown += Campo_KeyDown;
            disegno.Start();
            timer.Start();
            animazioni.Start();
            Controllo.Start();
            // aggiungo il componenti della trasmissione 
            ts.Children.Add(RotateTransform);
            ts.Children.Add(translateTransform);
            // aggiungo al disegno l'animazione a
            // creo i punti 
            double x = 0.2, y=0.4;

            r1.RenderTransform = ts;
            r1.RenderTransformOrigin = new Point(x, y);
            r2.RenderTransform = ts;
            r2.RenderTransformOrigin = new Point(x,y);
            r3.RenderTransform = ts;
            r3.RenderTransformOrigin = new Point(x, y);
            r4.RenderTransform = ts;
            r4.RenderTransformOrigin = new Point(x, y);
            //r5.RenderTransform = ts;
            r6.RenderTransform = ts;
            r6.RenderTransformOrigin = new Point(x, y);
            // calcolo transazione 
            translateTransform.X = +40;
           translateTransform.Y = +30;
        }

        private void Controllo_Tick(object sender, EventArgs e)
        {
            if (Canvas.GetLeft(player) >= Canvas.GetLeft(arrivo))
            {
                MessageBox.Show("Arrivato");
                System.Windows.Application.Current.Shutdown();
            }
            if (rotazione == true)
            {
                if((Canvas.GetLeft(player) + player.ActualWidth >= Canvas.GetLeft(r1))&&((Canvas.GetLeft(player) + player.ActualWidth <= Canvas.GetLeft(r1)+r1.ActualWidth))){
                    Canvas.SetLeft(player, Canvas.GetLeft(r1) - 60);
                }
                if ((Canvas.GetLeft(player) + player.ActualWidth >= Canvas.GetLeft(r2)) && ((Canvas.GetLeft(player) + player.ActualWidth <= Canvas.GetLeft(r2) + r2.ActualWidth)))
                {
                    Canvas.SetLeft(player, Canvas.GetLeft(r2) - 60);
                }
                if ((Canvas.GetLeft(player) + player.ActualWidth >= Canvas.GetLeft(r3)) && ((Canvas.GetLeft(player) + player.ActualWidth <= Canvas.GetLeft(r3) + r3.ActualWidth)))
                {
                    Canvas.SetLeft(player, Canvas.GetLeft(r3) - 60);
                }
                if ((Canvas.GetLeft(player) + player.ActualWidth >= Canvas.GetLeft(r4)) && ((Canvas.GetLeft(player) + player.ActualWidth <= Canvas.GetLeft(r4) + r4.ActualWidth)))
                {
                    Canvas.SetLeft(player, Canvas.GetLeft(r4) - 60);
                }
                if ((Canvas.GetLeft(player) + player.ActualWidth >= Canvas.GetLeft(r6)) && ((Canvas.GetLeft(player) + player.ActualWidth <= Canvas.GetLeft(r6) + r6.ActualWidth)))
                {
                    Canvas.SetLeft(player, Canvas.GetLeft(r6) - 60);
                }
                // bastardata indietro 
                
                if ((Canvas.GetLeft(player) <= Canvas.GetLeft(r1) + r1.ActualWidth)&&((Canvas.GetLeft(player) >= Canvas.GetLeft(r1))))
                {
                    Canvas.SetLeft(player, Canvas.GetLeft(r1) - player.ActualWidth);
                }
                
                if ((Canvas.GetLeft(player) <= Canvas.GetLeft(r2) + r2.ActualWidth)&&(Canvas.GetLeft(player) >= Canvas.GetLeft(r2)))
                {
                    Canvas.SetLeft(player, Canvas.GetLeft(r2) - player.ActualWidth);
                }
                
                if ((Canvas.GetLeft(player) <= Canvas.GetLeft(r3) + r3.ActualWidth)&&(Canvas.GetLeft(player) >= Canvas.GetLeft(r3)))
                {
                    Canvas.SetLeft(player, Canvas.GetLeft(r3) - player.ActualWidth);
                }
                if ((Canvas.GetLeft(player) <= Canvas.GetLeft(r4) + r4.ActualWidth)&&(Canvas.GetLeft(player) >= Canvas.GetLeft(r4)))
                {
                    Canvas.SetLeft(player, Canvas.GetLeft(r4) - player.ActualWidth);
                }
                if ((Canvas.GetLeft(player) <= Canvas.GetLeft(r6) + r6.ActualWidth)&&(Canvas.GetLeft(player) >= Canvas.GetLeft(r6)))
                {
                    Canvas.SetLeft(player, Canvas.GetLeft(r6) - player.ActualWidth);
                }
             //   */
            }

            // conrollo oggetti  
        }
         private void Animazioni_Tick(object sender, EventArgs e)
        {
           
            int altezza_rotazione = 140;

            r1.Height = altezza_rotazione;
            r2.Height = altezza_rotazione;
            r3.Height = altezza_rotazione;
            r4.Height = altezza_rotazione;
            r6.Height = altezza_rotazione;

            if (WindowState == WindowState.Maximized)
            {
                disegno.Stop();
            }
            RotateTransform.Angle +=90;
            if (RotateTransform.Angle == 360)
                RotateTransform.Angle = 0;

            if ((RotateTransform.Angle == 0) || (RotateTransform.Angle == 180))
            {/*
                r1.Height = altezza_rotazione;
                r2.Height = altezza_rotazione;
                r3.Height = altezza_rotazione;
                r4.Height = altezza_rotazione;
                r6.Height = altezza_rotazione;
                */
                rotazione = true;
            }
            else {
                /*
                r1.Height = altezza_rotazione;
                r2.Height = ;
                r3.Height = 50;
                r4.Height = 50;
                r6.Height = 50;
                */
                rotazione = false;
                // setto la lunghezza 
            }
            r1.Fill = Brushes.GreenYellow;
            r2.Fill = Brushes.DarkRed;
            r3.Fill = Brushes.IndianRed;
            r3.Fill = Brushes.BlueViolet;
            r4.Fill = Brushes.GhostWhite;
            r6.Fill = Brushes.SandyBrown;
            //RotateTransform.CenterX = -100;
        }
        private void Disegno_Tick(object sender, EventArgs e)
        {
            double c = 0;
            Canvas.SetLeft(arrivo, ActualWidth - arrivo.Width - 10);
            Canvas.SetTop(arrivo, ((ActualHeight / 2) - arrivo.Height+20));

            Canvas.SetLeft(player, 0 + 10);
            Canvas.SetTop(player, ((ActualHeight / 2) - player.Height));

            c = 70+200 ;
            double altezza = 60;
              //  player.ActualHeight + 20;
            Canvas.SetLeft(r5, 100);
            Canvas.SetTop(r5, ((ActualHeight / 2) - r1.Height));

            Canvas.SetLeft(r1, c);
            Canvas.SetTop(r1, ((ActualHeight / 2) - r1.Height+altezza));
            c += 230;
            Canvas.SetLeft(r2, c);
            Canvas.SetTop(r2, ((ActualHeight / 2) - r2.Height+altezza));
            c += 230;
            Canvas.SetLeft(r3, c );
            Canvas.SetTop(r3, ((ActualHeight / 2) - r2.Height+altezza));
            c += 230;
            Canvas.SetLeft(r4, c );
            Canvas.SetTop(r4, ((ActualHeight / 2) - r2.Height+altezza));
            c += 230;
            Canvas.SetLeft(r6, c);
            Canvas.SetTop(r6, ((ActualHeight / 2) - r2.Height+altezza));
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (Canvas.GetLeft(player) <= 0)
            {
                Canvas.SetLeft(player, 10);
            }
            if (Canvas.GetLeft(player) + player.ActualWidth >= ActualWidth)
            {
                Canvas.SetLeft(player, ActualWidth - player.ActualWidth - 10);
            }
        }

        private void Campo_KeyDown(object sender, KeyEventArgs e)
        {
            disegno.Stop();
            if (e.Key.Equals(Key.D))
            {
                Canvas.SetLeft(player, Canvas.GetLeft(player)+ 25);// sinstra
            }
            else
            {
                if (e.Key.Equals(Key.Right))
                {
                    Canvas.SetLeft(player, Canvas.GetLeft(player) + 25);
                }
            }
            if (e.Key.Equals(Key.A))
            {
                Canvas.SetLeft(player, Canvas.GetLeft(player) - 30);// destra
            }
            else
            {
                if (e.Key.Equals(Key.Left))
                {
                    Canvas.SetLeft(player, Canvas.GetLeft(player) - 30);
                }
            }
        }
    }
}